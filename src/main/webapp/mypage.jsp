<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <title>TomcatTask</title>
    <style><%@include file='css/style.css' %></style>
    <script><%@include file='js/mypage.js' %> </script>
</head>
<body>
<h1>Hello</h1>
<%--  Show table --%>
<div>
    <table>
        <caption hidden> Faculty information </caption>
        <tr>
            <th scope="col">Name Faculty</th>
            <th scope="col">FIO</th>
            <th scope="col">Phone</th>
            <th scope="col">Start Working</th>
            <th scope="col">Action</th>
        </tr>
        <c:forEach var="faculty" items="${faculty}">
            <tr>
                <td>${faculty.namefaculty}</td>
                <td><c:forEach var="person" items="${faculty.persons}">${person.fio} </c:forEach></td>
                <td>${faculty.phone}</td>
                <td><c:forEach var="person" items="${faculty.persons}">${person.startWorking} </c:forEach></td>
                <td><button onclick="location.href='http://localhost:8080/TomcatTask/edit?id=${faculty.id}'">Edit</button> / <button onclick="send(${faculty.id})">Delete</button></td>
            </tr>
        </c:forEach>
    </table>
</div>
<%-- Add Form --%>
<div class="container" >
    <div>
    <FORM action="" method="post" accept-charset="UTF-8">
        <INPUT TYPE="text" NAME="nameFaculty" class="nameFaculty" placeholder="Имя факультета">
        <select name="authors" multiple>
            <c:forEach var="person" items="${person}">
                <option value="${person.id}">${person.fio}</option>
            </c:forEach>
        </select>
        <INPUT TYPE="text" NAME="phone" class="phone" placeholder="Номер телефона">
        <input type="submit" value="Submit" />
    </FORM>
    </div>
<%-- Filter --%>
    <div>
    <h2>Filters</h2>
    <form method="post" action="filter">
        <select name="author">
            <option value=""></option>
            <c:forEach var="person" items="${person}">
                <option value="${person.id}">${person.fio}</option>
            </c:forEach>
        </select>
        <input type="submit" value="Filter" />
    </form>
        <button onclick="location.href='/TomcatTask'">Drop filter</button>
    </div>
</div>
</body>
</html>