<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <title>Title</title>
</head>
<body>
<form method="post" action="edit" style="margin-top: 2%;">
    <input type="hidden" name="id" value="${faculty.id}" />
    <INPUT TYPE="text" NAME="name" class="name" placeholder="Имя факультета">
    <select name="authors" multiple>
        <c:forEach var="person" items="${person}">
            <option value="${person.id}">${person.fio}</option>
        </c:forEach>
    </select>
    <INPUT TYPE="text" NAME="phone" class="phone" placeholder="Номер телефона">
    <input type="submit" value="Submit" />
    <a href="/TomcatTask">Back to main page</a>
</form>
</body>
</html>
