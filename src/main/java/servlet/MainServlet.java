package servlet;

import database.Faculty;
import database.Person;
import services.FacultyServices;
import services.PersonServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/")
public class MainServlet extends HttpServlet {
    private final FacultyServices facultyServices = new FacultyServices();
    private final PersonServices personServices = new PersonServices();
    static Logger logger;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            resp.setContentType("text/html;charset=utf-8");
            req.setAttribute("faculty", facultyServices.getAll());
            req.setAttribute("person", personServices.getAll());
            getServletContext().getRequestDispatcher("/mypage.jsp").forward(req, resp);
        } catch (ServletException | IOException e){
            logger.log(Level.INFO,"Exception in doGet MainServlet", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            resp.setContentType("text/html;charset=utf-8");
            String nameFaculty = req.getParameter("nameFaculty");
            String phone = req.getParameter("phone");
            String[] authorIDs = req.getParameterValues("authors");
            Faculty faculty = new Faculty(null, nameFaculty, phone, null);
            List<Person> personList = new ArrayList<>();
            for (String id : authorIDs) {
                personList.add(personServices.findById(Integer.parseInt(id)));
            }
            faculty.setPersons(personList);
            facultyServices.saveFaculty(faculty);
            resp.sendRedirect("/TomcatTask");
        } catch (NumberFormatException | IOException e){
            logger.log(Level.INFO,"Exception in doPost MainServlet", e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int deleteFacultyById;
        try{
        deleteFacultyById = Integer.parseInt(req.getParameter("id"));
//        facultyServices.deleteFacultyByID(deleteFacultyById); // Removes related values from the Faculty and Person tables
        facultyServices.remove(facultyServices.getById(deleteFacultyById)); // Removes values from the Faculty
        resp.sendRedirect("/TomcatTask");
        } catch (NumberFormatException | IOException e){
            logger.log(Level.INFO,"Exception in doDelete", e);
        }
    }
}