package servlet;

import database.Faculty;
import database.Person;
import services.FacultyServices;
import services.PersonServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/filter")
public class FilterServlet extends HttpServlet{
private final FacultyServices facultyServices = new FacultyServices();
private final PersonServices personServices = new PersonServices();
private static List<Faculty> filterFaculty;
    static Logger logger;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
        req.setAttribute("faculty", filterFaculty);
        req.setAttribute("person", personServices.getAll());
        getServletContext().getRequestDispatcher("/mypage.jsp").forward(req, resp);
        }
        catch (ServletException | IOException e){
            logger.log(Level.INFO,"Exception in doGet", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
        String personId = req.getParameter("author");
        filterFaculty = facultyServices.getAll();
        Person person = personServices.findById(Integer.parseInt(personId));
        filterFaculty.removeIf(faculty -> !faculty.getPersons().contains(person));
        doGet(req, resp);
        }
        catch (NumberFormatException | ServletException | IOException e){
            logger.log(Level.INFO,"Exception in doPost", e);
        }
    }
}
