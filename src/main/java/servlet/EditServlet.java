package servlet;

import database.Faculty;
import database.Person;
import services.FacultyServices;
import services.PersonServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/edit")
public class EditServlet extends HttpServlet {
    private final FacultyServices facultyServices = new FacultyServices();
    private final PersonServices personServices = new PersonServices();
    static Logger logger;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
        req.setAttribute("faculty", facultyServices.getById(Integer.parseInt(id)));
        req.setAttribute("person", personServices.getAll());
        getServletContext().getRequestDispatcher("/edit.jsp").forward(req, resp);
        } catch (NumberFormatException | ServletException | IOException e) {
            logger.log(Level.INFO,"Exception in doGet", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
        String idFaculty = req.getParameter("id");
        String nameFaculty = req.getParameter("name");
        String phoneFaculty = req.getParameter("phone");
        String[] authorIDs = req.getParameterValues("authors");
        Faculty faculty = new Faculty(Integer.parseInt(idFaculty),nameFaculty, phoneFaculty,null);
        List<Person> authors = new ArrayList<>();
        for(String id: authorIDs) {
            authors.add(personServices.findById(Integer.parseInt(id)));
        }
        faculty.setPersons(authors);
        int version= Integer.parseInt(idFaculty);
        faculty.setId(version);
        facultyServices.updateFaculty(faculty);
        resp.sendRedirect("/TomcatTask");
        } catch(NumberFormatException |IOException e){
            logger.log(Level.INFO,"Exception in doPost ", e);
        }

    }
}
