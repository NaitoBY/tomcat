package dao;

import database.Faculty;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;
import java.util.List;

public class FacultyDao {
    private Session session;

    public Faculty getById(int id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Faculty faculty = session.get(Faculty.class, id);
        session.close();
        return faculty;
    }
    public void save(Faculty faculty) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(faculty);
        transaction.commit();
        session.close();
    }
    public void remove(Faculty faculty) {
        setPersontWithSuchFacultyToNull(faculty);
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(faculty);
        transaction.commit();
        session.close();
    }
    public void setPersontWithSuchFacultyToNull(Faculty faculty) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        faculty.setPersons(null);
        session.merge(faculty);
        transaction.commit();
        session.close();
    }

    public void update(Faculty faculty) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(faculty);
        tx1.commit();
        session.close();
    }

    public List<Faculty> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("from Faculty").list();
    }
}
