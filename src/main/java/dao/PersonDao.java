package dao;

import database.Person;
import org.hibernate.Session;
import utils.HibernateSessionFactoryUtil;

import java.util.List;

public class PersonDao {
    private Session session;


    public Person findById(int id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Person person = session.get(Person.class, id);
        session.close();
        return person;
    }
    public List<Person> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("From Person").list();
    }

}
