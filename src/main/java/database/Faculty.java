package database;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Data
@Entity
@Table (name = "faculty")
public class Faculty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "namefaculty")
    private String namefaculty;
    @Column(name = "phone")
    private String phone;
    @ManyToMany (cascade = {
            CascadeType.ALL,
            CascadeType.MERGE
    })
    @JoinTable(name = "faculty_person",
            joinColumns = @JoinColumn(name = "id_faculty"),
            inverseJoinColumns = @JoinColumn(name = "id_person")
    )
    List<Person> persons;
}
