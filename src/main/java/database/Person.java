package database;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Data
@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fio")
    private String fio;
    @Column(name = "start_working")
    private Integer startWorking;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany (cascade = {
            CascadeType.ALL,
            CascadeType.MERGE
    })
    @JoinTable(name = "faculty_person",
            joinColumns = @JoinColumn(name = "id_person"),
            inverseJoinColumns = @JoinColumn(name = "id_faculty")
    )
    List<Faculty> faculty;
}
