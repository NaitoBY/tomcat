package services;

import dao.PersonDao;
import database.Person;

import java.util.List;

public class PersonServices {

    private PersonDao personDao = new PersonDao();

    public Person findById(int id) {
        return personDao.findById(id);
    }
    public List<Person> getAll() {
        return personDao.getAll();
    }


}
