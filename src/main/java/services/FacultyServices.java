package services;

import dao.FacultyDao;
import database.Faculty;

import java.util.List;

public class FacultyServices {

    private FacultyDao facultyDao = new FacultyDao();

    public Faculty getById(int id) {
        return facultyDao.getById(id);
    }
    public void remove(Faculty faculty) {
       facultyDao.remove(faculty);
    }

    public void saveFaculty(Faculty faculty) {
        facultyDao.save(faculty);
    }

    public void updateFaculty(Faculty faculty) {

        facultyDao.update(faculty);
    }
    public List<Faculty> getAll() {
        return facultyDao.getAll();
    }



}
